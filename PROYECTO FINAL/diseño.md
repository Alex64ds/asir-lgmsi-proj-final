# CONOCIENDO SCOUTS

## Idea de la página

Esta página se hablará del desarrollo del movimiento scout en la Comunidad Valenciana donde se hablará de los valores basicos que ha de tener un grupo scout así como sus integrantes, ya sea scout o scouter

## Contenido de la página

Esta página tendrá este contenido:

- Un apartado donde tendrá una explicación sobre como funcionan las distintas secciones en el escultismo

- Un calendario de acampadas, excursiones, actividades y eventos que se realizarán a lo largo del año

- Un apartado donde se conocerán detalles sobre los grupos scout de L'Horta sud (Nombre, ubicación, logo y colores de la pañoleta)

- Un contacto donde habrá algún formulario para apuntarse a cualquier grupo scout de esta comunidad y formar parte del movimiento scout

- Apartado que se explicará de las cosas más importantes de los scouts como lo son el saludo scout, las normas scout o su uniforme

## Diseño de la página
Aqui está el diseño general de la pagina:

![](./img_md/index_dis.png)

## Inspiraciones
El menu de la pagina me he inspirado del segundo enlace y la imagen de fondo con el texto del header me inspiré de la primera

### Enlaces 
https://demo.templatemonster.com/es/demo/51679.html  
        
https://es.wix.com/website-template/view/html/2954?originUrl=https%3A%2F%2Fes.wix.com%2Fwebsite%2Ftemplates%2Fhtml%2Fall%2F2&tpClick=view_button&esi=ba59dc9d-a500-451b-b2ab-29ca963326d8
